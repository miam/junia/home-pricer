# !/bin/bash
set -e

# Run migrations
if [ "$MIGRATE" == "true" ]; then
  echo "Check boot time..."
  bundle exec bumbler --all
  bundle exec bumbler --initializers --threshold=1
  echo "Run migrations..."
  bundle exec rails db:migrate
  echo "Migrations done"
fi

# Start Puma (in dev mode, starts normal rails server)
if [ "$RAILS_ENV" == "development" ]; then
  rm -f tmp/pids/server.pid && bundle exec rails server -p ${PORT:-3000} -b '0.0.0.0'
else
  # 4 workers with at least 2 threads
  bundle exec puma -w 4 -t 2:${RAILS_MAX_THREADS:-5} -p ${PORT:-3000}
fi