# frozen_string_literal: true

# Real estate transaction
class Transaction < ApplicationRecord
  belongs_to :city

  def update_from_record(record)
    update(
      transaction_date: record['fields']['date_mutation'],
      address: self.class.parse_address(record),
      estate_type: record['fields']['type_local'],
      total_area: record['fields']['surface_reelle_bati'],
      number_of_rooms: record['fields']['nombre_pieces_principales'],
      value: record['fields']['valeur_fonciere']
    )
  end

  def self.parse_address(record)
    record['fields'].slice('no_voie', 'type_de_voie', 'voie', 'code_postal', 'commune').values.join(' ')
  end
end
