# frozen_string_literal: true

require 'capybara'
require 'yaml'
require 'selenium/webdriver'
require 'browserstack/local'

# Standard configuration for local run on RackTest
# ---------------------------------------------------------------------

Capybara.server = :puma, { Silent: true }
Capybara.server_host = '0.0.0.0'
Capybara.server_port = '3010'
Capybara.app_host = 'http://0.0.0.0:3010'
# Capybara.run_server = false

# Selenium + Browserstack configuration
# ---------------------------------------------------------------------

# monkey patch to avoid reset sessions
module Capybara
  module Selenium
    class Driver < Capybara::Driver::Base
      def reset!
        @browser&.navigate&.to('about:blank')
      end
    end
  end
end

TASK_ID = (ENV['TASK_ID'] || 0).to_i
CONFIG = YAML.safe_load(File.read(File.join(File.dirname(__FILE__), 'browserstack.yml')))
CONFIG['user'] = ENV['BSTACK_USER'] || CONFIG['user']
CONFIG['key'] = ENV['BSTACK_KEY'] || CONFIG['key']

Capybara.register_driver :browserstack do |app|
  @caps = CONFIG['common_caps'].merge(CONFIG['browser_caps'][TASK_ID])
  @caps['build'] = ENV['CI_JOB_ID'] if ENV['CI_JOB_ID']

  # Code to start browserstack local before start of test
  if @caps['browserstack.local']&.to_s == 'true'
    @bs_local = BrowserStack::Local.new
    bs_local_args = { 'key' => CONFIG['key'].to_s }
    @bs_local.start(bs_local_args)
  end

  Capybara::Selenium::Driver.new(app,
                                 browser: :remote,
                                 url: "https://#{CONFIG['user']}:#{CONFIG['key']}@#{CONFIG['server']}/wd/hub",
                                 options: @caps)
end

Capybara.default_driver = :browserstack

# Code to stop browserstack local after end of test
at_exit do
  @bs_local&.stop
end
