# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Welcome index page', type: :feature do
  scenario 'show cities' do
    visit root_path
    expect(page).to have_content('Villes synchronisées :')

    page.percy_snapshot('Home - no city')
  end

  scenario 'sync city and go back' do
    visit root_path
    fill_in('Code postal', with: '59110')
    click_on('Synchroniser')
    expect(page).to have_content('Transactions enregistrées dans la commune : LA MADELEINE (59110)')
    page.percy_snapshot('Transactions - 59110')

    click_on('< Retour')
    expect(page).to have_content('Villes synchronisées :')
    expect(page).to have_content('59110 LA MADELEINE')
    page.percy_snapshot('Home - 59110')
  end
end
